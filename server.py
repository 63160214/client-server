import socket

# Read the contents of the file
with open("a.txt", "r") as file:
    file_contents = file.read()

# Create a socket object
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Bind the socket to a specific address and port
server.bind(("", 214))  # Use an available port

# Listen for incoming connections
server.listen(1)

print("Server is listening for connections...")

# Accept a connection from a client
client_socket, client_address = server.accept()

print(f"Accepted connection from: {client_address}")

# Send the file contents to the client
client_socket.sendall(file_contents.encode())

# Close the connection
client_socket.close()
server.close()
