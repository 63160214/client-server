import socket

target_host = "192.168.2.41"
target_port = 214  # Use the same port as in the server

# Create a socket object
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the client to the server
client.connect((target_host, target_port))

# Receive data from the server
file_contents = ""
while True:
    data = client.recv(1024).decode()
    if not data:
        break
    file_contents += data

# Close the connection
client.close()

# Print the received file contents
print("Received file contents:")
print(file_contents)
